package finki.ukim.mk.av1demoproject;

import finki.ukim.mk.av1demoproject.model.User;
import finki.ukim.mk.av1demoproject.model.enums.Role;
import finki.ukim.mk.av1demoproject.model.exceptions.InvalidArgumentsException;
import finki.ukim.mk.av1demoproject.model.exceptions.InvalidUsernameOrPasswordException;
import finki.ukim.mk.av1demoproject.model.exceptions.PasswordsDoNotMatchException;
import finki.ukim.mk.av1demoproject.model.exceptions.UsernameExistsException;
import finki.ukim.mk.av1demoproject.repository.jpa.UserRepository;
import finki.ukim.mk.av1demoproject.service.UserService;
import finki.ukim.mk.av1demoproject.service.impl.UserServiceImpl;
import org.checkerframework.checker.nullness.Opt;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserRegistrationTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    private UserService service;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        User user = new User("username","password","name","surname", Role.ROLE_USER);
        Mockito.when(this.userRepository.save(Mockito.any(User.class))).thenReturn(user);
        Mockito.when(this.passwordEncoder.encode(Mockito.anyString())).thenReturn("password");

        this.service = Mockito.spy(new UserServiceImpl(this.userRepository,this.passwordEncoder));
    }

    @Test
    public void testSuccessRegister() throws PasswordsDoNotMatchException, InvalidArgumentsException {
        User user = this.service.register("username","password","password","name","surname", Role.ROLE_USER);

        Mockito.verify(this.service).register("username","password","password","name","surname", Role.ROLE_USER);

        Assert.assertNotNull("User is null",user);

        Assert.assertEquals("name does not match","name",user.getName());
        Assert.assertEquals("password does not match","password",user.getPassword());
        Assert.assertEquals("username does not match","username",user.getUsername());
        Assert.assertEquals("surname does not match","surname",user.getSurname());
        Assert.assertEquals("role does not match",Role.ROLE_USER,user.getRole());
    }

    @Test
    public void testNullUsername() throws PasswordsDoNotMatchException, InvalidArgumentsException {
        Assert.assertThrows("InvalidUsernameOrPasswordException expected", InvalidUsernameOrPasswordException.class,
                () -> this.service.register(null,"password","password","name","surname", Role.ROLE_USER));
        Mockito.verify(this.service).register(null,"password","password","name","surname", Role.ROLE_USER);
    }

    @Test
    public void TestEmptyUsername() throws PasswordsDoNotMatchException, InvalidArgumentsException {
        String username = "";
        Assert.assertThrows("InvalidUsernameOrPasswordException expected",InvalidUsernameOrPasswordException.class,
                () -> this.service.register(username,"password","password","name","surname", Role.ROLE_USER));
        Mockito.verify(this.service).register(username,"password","password","name","surname", Role.ROLE_USER);
    }

    @Test
    public void TestEmptyPassword() throws PasswordsDoNotMatchException, InvalidArgumentsException {
        String username = "username";
        String password = "";
        Assert.assertThrows("InvalidUsernameOrPasswordException expected",InvalidUsernameOrPasswordException.class,
                () -> this.service.register(username,password,"password","name","surname", Role.ROLE_USER));
        Mockito.verify(this.service).register(username,password,"password","name","surname", Role.ROLE_USER);
    }

    @Test
    public void TestNullPassword() throws PasswordsDoNotMatchException, InvalidArgumentsException {
        String username = "username";
        String password = null;
        Assert.assertThrows("InvalidUsernameOrPasswordException expected",InvalidUsernameOrPasswordException.class,
                () -> this.service.register(username,password,"password","name","surname", Role.ROLE_USER));
        Mockito.verify(this.service).register(username,password,"password","name","surname", Role.ROLE_USER);
    }

    @Test
    public void TestPasswordMismatch() throws PasswordsDoNotMatchException, InvalidArgumentsException {
        String username = "username";
        String password = "password";
        String confirmPassword = "otherPassword";
        Assert.assertThrows("PasswordsDoNotMatchException expected",PasswordsDoNotMatchException.class,
                () -> this.service.register(username,password,confirmPassword,"name","surname", Role.ROLE_USER));
        Mockito.verify(this.service).register(username,password,confirmPassword,"name","surname", Role.ROLE_USER);
    }

    @Test
    public void testDuplicateUsername() throws PasswordsDoNotMatchException, InvalidArgumentsException {
        User user = new User("username","password","name","surname", Role.ROLE_USER);
        Mockito.when(this.userRepository.findByUsername(Mockito.anyString())).thenReturn(Optional.of(user));
        String username = "username";
        Assert.assertThrows("UsernameExistsException expected",UsernameExistsException.class,
                () -> this.service.register(username,"password","password","name","surname", Role.ROLE_USER));
        Mockito.verify(this.service).register(username,"password","password","name","surname", Role.ROLE_USER);
    }
}
