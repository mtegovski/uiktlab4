package finki.ukim.mk.av1demoproject.repository.impl;

import finki.ukim.mk.av1demoproject.bootstrap.DataHolder;
import finki.ukim.mk.av1demoproject.model.Category;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class InMemoryCategoryRepository {

    public List<Category> findAll(){
        return DataHolder.categories;
    }

    public Category save(Category c) {
        if(c== null || c.getName().isEmpty()) {
            return null;
        }
        DataHolder.categories.removeIf(category -> category.getName().equals(c.getName()));
        DataHolder.categories.add(c);
        return c;
    }

    public Optional<Category> findByName(String name) {
        return DataHolder.categories.stream().filter(category -> category.getName().equals(name)).findFirst();
    }

    public Optional<Category> findById(Long id) {
        return DataHolder.categories.stream().filter(category -> category.getId().equals(id)).findFirst();
    }

    public List<Category> search(String searchField) {
        return DataHolder.categories.stream().filter(category -> category.getName().contains(searchField)).collect(Collectors.toList());
    }

    public void delete(String name){
        DataHolder.categories.removeIf(category -> category.getName().equals(name));
    }

}
