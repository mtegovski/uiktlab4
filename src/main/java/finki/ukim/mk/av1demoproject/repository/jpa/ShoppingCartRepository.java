package finki.ukim.mk.av1demoproject.repository.jpa;

import finki.ukim.mk.av1demoproject.model.ShoppingCart;
import finki.ukim.mk.av1demoproject.model.User;
import finki.ukim.mk.av1demoproject.model.enums.ShoppingCartStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart,Long> {
    Optional<ShoppingCart> findByUserAndStatus(User user, ShoppingCartStatus status);
}
