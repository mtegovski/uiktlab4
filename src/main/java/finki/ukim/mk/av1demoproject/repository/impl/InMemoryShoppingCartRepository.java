package finki.ukim.mk.av1demoproject.repository.impl;


import finki.ukim.mk.av1demoproject.bootstrap.DataHolder;
import finki.ukim.mk.av1demoproject.model.ShoppingCart;
import finki.ukim.mk.av1demoproject.model.enums.ShoppingCartStatus;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class InMemoryShoppingCartRepository {

    public Optional<ShoppingCart> findById(Long id) {
        return DataHolder.shoppingCarts.stream().filter(cart -> cart.getId().equals(id)).findFirst();
    }

    public Optional<ShoppingCart> findByUsernameAndStatus(String username, ShoppingCartStatus status) {
        return DataHolder.shoppingCarts.stream()
                .filter(cart -> cart.getUser().getUsername().equals(username) && cart.getStatus().equals(status))
                .findFirst();
    }

    public ShoppingCart save(ShoppingCart shoppingCart) {
        DataHolder.shoppingCarts.removeIf(cart -> cart.getUser().getUsername().equals(shoppingCart.getUser().getUsername()));
        DataHolder.shoppingCarts.add(shoppingCart);
        return shoppingCart;
    }
}
