package finki.ukim.mk.av1demoproject.service;


import finki.ukim.mk.av1demoproject.model.User;
import finki.ukim.mk.av1demoproject.model.exceptions.InvalidArgumentsException;
import finki.ukim.mk.av1demoproject.model.exceptions.InvalidUserCredentialsException;
import finki.ukim.mk.av1demoproject.model.exceptions.PasswordsDoNotMatchException;

public interface AuthService {
    User login(String username, String password) throws InvalidUserCredentialsException, InvalidArgumentsException;


}
