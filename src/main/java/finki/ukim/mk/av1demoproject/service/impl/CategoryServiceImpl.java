package finki.ukim.mk.av1demoproject.service.impl;


import finki.ukim.mk.av1demoproject.model.Category;
import finki.ukim.mk.av1demoproject.repository.impl.InMemoryCategoryRepository;
import finki.ukim.mk.av1demoproject.repository.jpa.CategoryRepository;
import finki.ukim.mk.av1demoproject.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository _categoryRepository) {
        this.categoryRepository = _categoryRepository;
    }

    @Override
    public Category create(String name) {
        if(name == null || name.isEmpty()) {
            throw new IllegalCallerException();
        }
        Category c = new Category(name);
        categoryRepository.save(c);
        return c;
    }

    @Override
    public Category update(String name) {
        if(name == null || name.isEmpty()) {
            throw new IllegalCallerException();
        }
        Category c = new Category(name);
        categoryRepository.save(c);
        return c;
    }

    @Override
    public void delete(String name) {
        if(name == null || name.isEmpty()) {
            throw new IllegalCallerException();
        }
        categoryRepository.deleteByName(name);
    }

    @Override
    public List<Category> listCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public List<Category> searchCategories(String searchField) {
        return categoryRepository.findAllByNameLike(searchField);
    }
}
