package finki.ukim.mk.av1demoproject.service.impl;

import finki.ukim.mk.av1demoproject.model.Product;
import finki.ukim.mk.av1demoproject.model.ShoppingCart;
import finki.ukim.mk.av1demoproject.model.User;
import finki.ukim.mk.av1demoproject.model.enums.ShoppingCartStatus;
import finki.ukim.mk.av1demoproject.model.exceptions.ProductInCartException;
import finki.ukim.mk.av1demoproject.model.exceptions.ProductNotFoundException;
import finki.ukim.mk.av1demoproject.model.exceptions.ShoppingCartNotFoundException;
import finki.ukim.mk.av1demoproject.model.exceptions.UserNotFoundException;
import finki.ukim.mk.av1demoproject.repository.jpa.ShoppingCartRepository;
import finki.ukim.mk.av1demoproject.repository.jpa.UserRepository;
import finki.ukim.mk.av1demoproject.service.ProductService;
import finki.ukim.mk.av1demoproject.service.ShoppingCartService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;
    private final UserRepository userRepository;
    private final ProductService productService;

    public ShoppingCartServiceImpl(ShoppingCartRepository shoppingCartRepository, UserRepository userRepository, ProductService productService) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.userRepository = userRepository;
        this.productService = productService;
    }

    @Override
    public List<Product> listAllProducts(Long cartId) {
        if(this.shoppingCartRepository.findById(cartId).isEmpty()) {
            throw new ShoppingCartNotFoundException(cartId);
        }
        return this.shoppingCartRepository.findById(cartId).get().getProducts();
    }

    @Override
    public ShoppingCart getActiveShoppingCart(String username) {
        User user = this.userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));

        return this.shoppingCartRepository.findByUserAndStatus(user, ShoppingCartStatus.CREATED)
                .orElseGet(() -> {
                    ShoppingCart shoppingCart = new ShoppingCart(user);
                    return this.shoppingCartRepository.save(shoppingCart);
                });
    }

    @Override
    public ShoppingCart addProductToShoppingCart(String username, Long productId) {
        ShoppingCart shoppingCart = this.getActiveShoppingCart(username);
        Product product = this.productService.findById(productId).orElseThrow(() -> new ProductNotFoundException(productId));
        if(shoppingCart.getProducts()
                .stream().filter(p->p.getId().equals(productId))
                .collect(Collectors.toList()).size() > 0) {
            throw new ProductInCartException(productId);
        }
        shoppingCart.getProducts().add(product);
        return this.shoppingCartRepository.save(shoppingCart);
    }
}
