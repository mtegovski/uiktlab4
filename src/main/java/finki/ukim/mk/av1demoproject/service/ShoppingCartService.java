package finki.ukim.mk.av1demoproject.service;

import finki.ukim.mk.av1demoproject.model.Product;
import finki.ukim.mk.av1demoproject.model.ShoppingCart;

import java.util.List;

public interface ShoppingCartService {
    List<Product> listAllProducts(Long cartId);
    ShoppingCart getActiveShoppingCart(String username);
    ShoppingCart addProductToShoppingCart(String username, Long productId);
}
