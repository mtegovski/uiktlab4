package finki.ukim.mk.av1demoproject.service.impl;

import finki.ukim.mk.av1demoproject.model.User;
import finki.ukim.mk.av1demoproject.model.exceptions.InvalidArgumentsException;
import finki.ukim.mk.av1demoproject.model.exceptions.InvalidUserCredentialsException;
import finki.ukim.mk.av1demoproject.model.exceptions.PasswordsDoNotMatchException;
import finki.ukim.mk.av1demoproject.model.exceptions.UsernameExistsException;
import finki.ukim.mk.av1demoproject.repository.impl.InMemoryUserRepository;
import finki.ukim.mk.av1demoproject.repository.jpa.UserRepository;
import finki.ukim.mk.av1demoproject.service.AuthService;
import org.springframework.stereotype.Service;


@Service
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;

    public AuthServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User login(String username, String password) throws InvalidUserCredentialsException, InvalidArgumentsException {
        if(username.isEmpty() || password.isEmpty())
            throw new InvalidArgumentsException();
        return userRepository.findByUsernameAndPassword(username,password).orElseThrow(InvalidUserCredentialsException::new);
    }

}
