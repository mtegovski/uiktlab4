package finki.ukim.mk.av1demoproject.service.impl;

import finki.ukim.mk.av1demoproject.model.Category;
import finki.ukim.mk.av1demoproject.model.Manufacturer;
import finki.ukim.mk.av1demoproject.model.Product;
import finki.ukim.mk.av1demoproject.model.exceptions.CategoryNotFoundException;
import finki.ukim.mk.av1demoproject.model.exceptions.ManufacturerNotFoundException;
import finki.ukim.mk.av1demoproject.repository.jpa.CategoryRepository;
import finki.ukim.mk.av1demoproject.repository.jpa.ManufacturerRepository;
import finki.ukim.mk.av1demoproject.repository.jpa.ProductRepository;
import finki.ukim.mk.av1demoproject.service.ProductService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ManufacturerRepository manufacturerRepository;

    public ProductServiceImpl(ProductRepository inMemoryProductRepository,
                              CategoryRepository inMemoryCategoryRepository,
                              ManufacturerRepository inMemoryManufacturerRepository) {
        this.productRepository = inMemoryProductRepository;
        this.categoryRepository = inMemoryCategoryRepository;
        this.manufacturerRepository = inMemoryManufacturerRepository;
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public Optional<Product> findByName(String name) {
        return productRepository.findByName(name);
    }

    @Override
    @Transactional
    public Optional<Product> save(String name, Double price, Integer quantity, Long categoryId, Long manufacturerId) {
        Category category = this.categoryRepository.findById(categoryId)
                .orElseThrow(() -> new CategoryNotFoundException(categoryId));
        Manufacturer manufacturer = this.manufacturerRepository.findById(manufacturerId)
                .orElseThrow(() -> new ManufacturerNotFoundException(manufacturerId));

        this.productRepository.deleteByName(name);
        return Optional.of(this.productRepository.save(new Product(name, price, quantity, category, manufacturer)));
    }


    @Override
    public void delete(Long id) {
        productRepository.deleteById(id);
    }
}
