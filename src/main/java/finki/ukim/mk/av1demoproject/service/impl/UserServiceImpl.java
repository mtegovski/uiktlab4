package finki.ukim.mk.av1demoproject.service.impl;

import finki.ukim.mk.av1demoproject.model.User;
import finki.ukim.mk.av1demoproject.model.enums.Role;
import finki.ukim.mk.av1demoproject.model.exceptions.InvalidArgumentsException;
import finki.ukim.mk.av1demoproject.model.exceptions.InvalidUsernameOrPasswordException;
import finki.ukim.mk.av1demoproject.model.exceptions.PasswordsDoNotMatchException;
import finki.ukim.mk.av1demoproject.model.exceptions.UsernameExistsException;
import finki.ukim.mk.av1demoproject.repository.jpa.UserRepository;
import finki.ukim.mk.av1demoproject.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User register(String username, String password, String confirmPassword, String name, String surname, Role role) throws InvalidArgumentsException, PasswordsDoNotMatchException {
        if(username == null || username.isEmpty() || password == null || password.isEmpty()) {
            throw new InvalidUsernameOrPasswordException();
        }
        if(!password.equals(confirmPassword)){
            throw new PasswordsDoNotMatchException();
        }
        if(this.userRepository.findByUsername(username).isPresent()) {
            throw new UsernameExistsException(username);
        }
        User user = new User(username,passwordEncoder.encode(password),name,surname,role);
        return this.userRepository.save(user);

    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return this.userRepository.findByUsername(s).orElseThrow(() -> new UsernameNotFoundException(s));
    }
}
