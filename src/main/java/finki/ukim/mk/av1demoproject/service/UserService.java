package finki.ukim.mk.av1demoproject.service;

import finki.ukim.mk.av1demoproject.model.User;
import finki.ukim.mk.av1demoproject.model.enums.Role;
import finki.ukim.mk.av1demoproject.model.exceptions.InvalidArgumentsException;
import finki.ukim.mk.av1demoproject.model.exceptions.PasswordsDoNotMatchException;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User register(String username, String password, String confirmPassword, String name, String surname, Role role) throws InvalidArgumentsException, PasswordsDoNotMatchException;

}
