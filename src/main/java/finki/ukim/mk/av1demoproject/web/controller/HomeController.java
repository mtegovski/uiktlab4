package finki.ukim.mk.av1demoproject.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/")
public class HomeController {

    @GetMapping(value = {"home",""})
    public String getHomePage(Model model) {
        model.addAttribute("bodyContent","home");
        return "master-template";
    }

    @GetMapping("access_denied")
    public String accessDenied() {

        return "access_denied";
    }


}
