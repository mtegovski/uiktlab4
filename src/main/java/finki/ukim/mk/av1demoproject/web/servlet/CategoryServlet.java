package finki.ukim.mk.av1demoproject.web.servlet;


import finki.ukim.mk.av1demoproject.model.Category;
import finki.ukim.mk.av1demoproject.service.CategoryService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "category-servlet", urlPatterns = "/servlet/category")
public class CategoryServlet extends HttpServlet {

    private final CategoryService categoryService;

    public CategoryServlet(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter = resp.getWriter();
        printWriter.write("<html>");
        printWriter.write("<head>");
        printWriter.write("</head>");
        printWriter.write("<body>");
        printWriter.write("<h3>Categories</h3>");
        printWriter.write("<ul>");
        categoryService.listCategories().forEach(category -> {
            printWriter.format("<li>%s</li>", category.getName());
        });
        printWriter.write("</ul>");
        printWriter.println("<h2>Add New Category</h2>");

        printWriter.println("<form method='POST' action='/servlet/category'/>");
        printWriter.println("<label for='name'>Name:<label>");
        printWriter.println("<input id='name' type='text' name='name'/>");
        printWriter.println("<input type='submit' value='Submit'/>");
        printWriter.println("</form>");
        printWriter.write("</body>");
        printWriter.write("</html>");
        printWriter.flush();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String categoryName = req.getParameter("name");
         categoryService.create(categoryName);
        resp.sendRedirect("/servlet/category");
    }

}
