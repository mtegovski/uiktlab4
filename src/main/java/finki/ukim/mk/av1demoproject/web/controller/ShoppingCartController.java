package finki.ukim.mk.av1demoproject.web.controller;

import finki.ukim.mk.av1demoproject.model.ShoppingCart;
import finki.ukim.mk.av1demoproject.model.User;
import finki.ukim.mk.av1demoproject.service.ShoppingCartService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/shopping-cart")
public class ShoppingCartController {

    private final ShoppingCartService shoppingCartService;

    public ShoppingCartController(ShoppingCartService shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }

    @GetMapping
    public String getShoppingCartPage(@RequestParam(required = false) String error, HttpServletRequest request, Model model) {
        if(error != null && !error.isEmpty()) {
            model.addAttribute("hasError",true);
            model.addAttribute("error",error);
            //return "shopping-cart?error=" + error;
        }
        String username = request.getRemoteUser();
        ShoppingCart shoppingCart = this.shoppingCartService.getActiveShoppingCart(username);
        model.addAttribute("products",this.shoppingCartService.listAllProducts(shoppingCart.getId()));
        model.addAttribute("bodyContent","shopping-cart");
        return "master-template";
    }

    @PostMapping("/add-product/{id}")
    public String addProductToCart(@PathVariable Long id,HttpServletRequest request) {
        try {
            String username = request.getRemoteUser();
            ShoppingCart shoppingCart = this.shoppingCartService.addProductToShoppingCart(username,id);
            return "redirect:/shopping-cart";
        }
        catch (RuntimeException ex) {
            return "redirect:/shopping-cart?error=" + ex.getMessage();
        }
    }
}
