package finki.ukim.mk.av1demoproject.web.controller;

import finki.ukim.mk.av1demoproject.model.User;
import finki.ukim.mk.av1demoproject.model.exceptions.InvalidArgumentsException;
import finki.ukim.mk.av1demoproject.model.exceptions.InvalidUserCredentialsException;
import finki.ukim.mk.av1demoproject.service.AuthService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/login")
public class LoginController {

    private final AuthService authService;

    public LoginController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping()
    public String getLoginPage(Model model)
    {
        model.addAttribute("bodyContent","login");
        return "master-template";
    }

    @PostMapping
    public String login(HttpServletRequest request, Model model) {
        User user;
        try {
            user = authService.login(request.getParameter("username"), request.getParameter("password"));
            request.getSession().setAttribute("user",user);
            return "redirect:/home";
        }
        catch (InvalidUserCredentialsException | InvalidArgumentsException e) {
            model.addAttribute("hasError",true);
            model.addAttribute("error",e.getMessage());
            return "login";
        }
    }

}
