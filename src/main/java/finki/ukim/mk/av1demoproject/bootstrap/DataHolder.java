package finki.ukim.mk.av1demoproject.bootstrap;


import finki.ukim.mk.av1demoproject.model.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataHolder {
    public static List<Category> categories = new ArrayList<>();
    public static List<User> users = new ArrayList<>();
    public static List<Manufacturer> manufacturers = new ArrayList<>();
    public static List<Product> products = new ArrayList<>();
    public static List<ShoppingCart> shoppingCarts = new ArrayList<>();

//    @PostConstruct
//    public void init(){
//        categories.add(new Category("Software"));
//        categories.add(new Category("Sport"));
//
//        users.add(new User("Mikuleee","mt","Mihail","Tegovski"));
//        users.add(new User("Foronisus","zm","Zanko","Mojsovski"));
//
//        manufacturers.add(new Manufacturer("Nike","Nike Home"));
//        manufacturers.add(new Manufacturer("Adidas","Adidas Home"));
//
//        products.add(new Product("Ball",250.0,10,categories.get(1),manufacturers.get(1)));
//        products.add(new Product("Shoes",100.0,5,categories.get(1),manufacturers.get(0)));
//    }
}
