package finki.ukim.mk.av1demoproject.model.exceptions;

public class InvalidArgumentsException extends Exception{

    public InvalidArgumentsException() {
        super("Invalid arguments exception");
    }
}
