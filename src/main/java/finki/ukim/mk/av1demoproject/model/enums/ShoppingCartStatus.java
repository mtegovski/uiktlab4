package finki.ukim.mk.av1demoproject.model.enums;

public enum ShoppingCartStatus {
    CREATED,
    CANCELED,
    FINISHED
}
