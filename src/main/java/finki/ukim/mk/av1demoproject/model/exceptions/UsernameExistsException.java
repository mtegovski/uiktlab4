package finki.ukim.mk.av1demoproject.model.exceptions;

public class UsernameExistsException extends RuntimeException{
    public UsernameExistsException(String username) {
        super(String.format("Product with username %s already exists",username));
    }
}
