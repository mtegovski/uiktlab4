package finki.ukim.mk.av1demoproject.model.exceptions;

public class PasswordsDoNotMatchException extends Exception{
    public PasswordsDoNotMatchException() {
        super("Passwords do not match exception");
    }
}
