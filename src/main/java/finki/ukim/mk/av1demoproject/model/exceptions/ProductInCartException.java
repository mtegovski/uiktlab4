package finki.ukim.mk.av1demoproject.model.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
public class ProductInCartException extends RuntimeException{
    public ProductInCartException(Long id) {
        super(String.format("Product with id %d is already in the cart!",id));
    }
}
