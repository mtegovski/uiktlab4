package finki.ukim.mk.av1demoproject.model.exceptions;

public class InvalidUserCredentialsException extends Exception{
    public InvalidUserCredentialsException() {
        super("Invalid user credentials");
    }
}
